//console.log("Hello World");

function printSum(num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}
printSum(5, 15);


function printDifference(num1, num2){
	let difference = num1 - num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(difference);
}
printDifference(20, 5);


function returnProduct(num1, num2){
	return num1 * num2;
}
let product = returnProduct(50, 10);
console.log("The product of 50 and 10: ");
console.log(product);


function returnQuotient(num1, num2){
	return num1 / num2;
}
let quotient = returnQuotient(50, 10);
console.log("The product of 50 and 10: ");
console.log(quotient);


function returnCircleArea(radius){
	return radius * radius * 3.1416;
}
let circleArea = returnCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius:");
console.log(circleArea);


function returnAverage(num1, num2, num3, num4){
	
	return (num1 + num2 + num3 + num4) / 4;
}
let averageVar = returnAverage(20, 40, 60, 80);
console.log("The average of 20, 40, 60 and 80:");
console.log(averageVar);


function returnPercentage(score, total){
	return (score / total) * 100;
}
let percentage = returnPercentage(38, 50);
console.log("Is 38/50 a passing score?");
let isPassed = percentage >= 75;
console.log(isPassed);
